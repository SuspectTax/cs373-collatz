#!/usr/bin/env python3

# ----------
# MakeTests.py
# ----------

import random
from typing import IO, List
from io       import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# ----------
# make_tests
# ----------

def make_tests (i: int) -> None :
    testFileName= 'CollatzTests.txt'
    testFile=open(testFileName, 'a')
    for j in range(0,i):
        
        firstNum=random.randint(1,1000000)
        secondNum=random.randint(firstNum,1000000)
        solution=collatz_eval(firstNum,secondNum)
        testFile.write('    def test_eval_'+str(j)+' (self) :\n')
        testFile.write('        v = collatz_eval('+str(firstNum)+', '+str(secondNum)+')\n')
        testFile.write('        self.assertEqual(v, '+str(solution)+')\n\n')

def main():
    make_tests(500)

if __name__=="__main__":
    main()
    
    
