#!/usr/bin/env python3

# --------------
# TestCollatz.py
# --------------

"""
Runs unit and acceptance tests on Collatz.py
"""

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase
from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz(TestCase):
    """
    Class containing all necessary unit and acceptance tests
    """

    # ----
    # read
    # ----

    def test_read(self):
        """
        Read Test
        """
        input_string = "1 10\n"
        range_beg, range_end = collatz_read(input_string)
        self.assertEqual(range_beg, 1)
        self.assertEqual(range_end, 10)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        """
        Unit Test 1
        """
        result = collatz_eval(1, 10)
        self.assertEqual(result, 20)

    def test_eval_2(self):
        """
        Unit Test 2
        """
        result = collatz_eval(100, 200)
        self.assertEqual(result, 125)

    def test_eval_3(self):
        """
        Unit Test 3
        """
        result = collatz_eval(201, 210)
        self.assertEqual(result, 89)

    def test_eval_4(self):
        """
        Unit Test 4
        """
        result = collatz_eval(900, 1000)
        self.assertEqual(result, 174)

    def test_eval_5(self):
        """
        Unit Test 5
        """
        result = collatz_eval(55964, 958491)
        self.assertEqual(result, 525)

    def test_eval_6(self):
        """
        Unit Test 6
        """
        result = collatz_eval(895095, 944096)
        self.assertEqual(result, 507)

    def test_eval_7(self):
        """
        Unit Test 7
        """
        result = collatz_eval(434002, 516490)
        self.assertEqual(result, 470)

    def test_eval_8(self):
        """
        Unit Test 8
        """
        result = collatz_eval(494667, 897794)
        self.assertEqual(result, 525)

    def test_eval_9(self):
        """
        Unit Test 9
        """
        result = collatz_eval(593909, 620181)
        self.assertEqual(result, 447)

    # -----
    # print
    # -----

    def test_print(self):
        """
        Print Test
        """
        write = StringIO()
        collatz_print(write, 1, 10, 20)
        self.assertEqual(write.getvalue(), "1 10 20\n")

    # -----
    # solve
    # -----
    def test_solve(self):
        """
        Input Test
        """
        read = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        write = StringIO()
        collatz_solve(read, write)
        self.assertEqual(
            write.getvalue(),
            "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    # ---------------------
    # Additional Eval Tests
    # ---------------------

    def test_eval_10(self):
        """
        Unit Test 10
        """
        result = collatz_eval(659505, 959399)
        self.assertEqual(result, 525)

    def test_eval_11(self):
        """
        Unit Test 11
        """
        result = collatz_eval(833102, 972263)
        self.assertEqual(result, 525)

    def test_eval_12(self):
        """
        Unit Test 12
        """
        result = collatz_eval(755086, 976739)
        self.assertEqual(result, 525)

    def test_eval_13(self):
        """
        Unit Test 13
        """
        result = collatz_eval(883455, 986952)
        self.assertEqual(result, 507)

    def test_eval_14(self):
        """
        Unit Test 14
        """
        result = collatz_eval(177778, 233196)
        self.assertEqual(result, 443)

    def test_eval_15(self):
        """
        Unit Test 15
        """
        result = collatz_eval(207250, 870686)
        self.assertEqual(result, 525)

    def test_eval_16(self):
        """
        Unit Test 16
        """
        result = collatz_eval(795534, 960112)
        self.assertEqual(result, 525)

    def test_eval_17(self):
        """
        Unit Test 17
        """
        result = collatz_eval(62039, 515575)
        self.assertEqual(result, 470)


# ----
# main
# ----
if __name__ == "__main__":  # pragma: no cover
    main()
