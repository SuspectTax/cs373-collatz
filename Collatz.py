#!/usr/bin/env python3

# ----------
# Collatz.py
# ----------
"""
Algorithm for accepting 2 numbers of user input and returning
the largest cycle length of all numbers in that range according
to the Collatz Conjecture
"""

from typing import IO, List

# ------------
# collatz_read
# ------------

# Initialize the cache with the cycle length recursive base case
cycle_cache = dict([(1, 1)])


def collatz_read(input_string: str) -> List[int]:
    """
    read two ints
    input_string a string
    return a list of two ints, representing the beginning and end of a range, [i, j]
    """
    input_range = input_string.split()
    return [int(input_range[0]), int(input_range[1])]

# ------------
# collatz_eval
# ------------


def collatz_eval(range_beg: int, range_end: int) -> int:
    """
    range_beg the beginning of the range, inclusive
    range_end the end       of the range, inclusive
    return the max cycle length of the range [range_beg, range_end]
    """
    # Preconditions
    assert range_beg > 0
    assert range_beg < 1000000
    assert range_end > 0
    assert range_end < 1000000
    # Initialize minimum cycle length
    max_cycle = 1
    # max and min implemented to account for unusual input if i>j
    start = min(range_beg, range_end)
    stop = max(range_beg + 1, range_end + 1)
    mod_9_start = int(start * 1.5) + 9
    # Max cycle length only found in upper half of b bound, reduces
    # unnecessary look ups and computations
    start = max(start, stop // 2)
    if mod_9_start > stop:
        mod_9_start = stop
    mod_9_start = mod_9_start - mod_9_start % 9

    # Iterate through small ranges to avoid mod 9 size conflicts
    if stop - start < 40:
        for num in range(start, stop):
            max_cycle = max(max_cycle, cycle_length(num))
    else:
        # Iterate through range up to mod 9 start to ensure all items larger
        # than mod 9 items found
        for num in range(start, mod_9_start + 1):
            max_cycle = max(max_cycle, cycle_length(num))
        # Iterate through mod 9 items, those left out are mathematically
        # smaller than other cycle lengths preceding it
        for num in range(mod_9_start, stop - 20, 9):
            max_cycle = max(max_cycle, cycle_length(num))
            max_cycle = max(max_cycle, cycle_length(num + 1))
            max_cycle = max(max_cycle, cycle_length(num + 3))
            max_cycle = max(max_cycle, cycle_length(num + 6))
            max_cycle = max(max_cycle, cycle_length(num + 7))
        # Iterate through final few elements in range to avoid overshooting
        # range with mod 9 iteration
        for num in range(stop - 30, stop):
            max_cycle = max(max_cycle, cycle_length(num))
    # Postconditions
    assert max_cycle > 0
    return max_cycle


def cycle_length(i: int) -> int:
    """
    i the number you wish to determine the cycle length of
    determines cycle length of an element
    """
    # Check if cycle length is already in cache
    if i not in cycle_cache:
        # Evaluate its cycle length recursively by tracing through the cache
        # Increment cycle length by 2 and go to (3n+1)/2 or by 1 and go to n/2
        cycle_cache[i] = (cycle_length(i + int(i >> 1) + 1) +
                          2 if i % 2 == 1 else cycle_length(i // 2) + 1)
    return cycle_cache.get(i)


# -------------
# collatz_print
# -------------

def collatz_print(
        write: IO[str],
        range_beg: int,
        range_end: int,
        max_cycle: int) -> None:
    """
    print three ints
    write a writer
    range_beg the beginning of the range, inclusive
    range_end the end       of the range, inclusive
    max_cycle the max cycle length
    """
    write.write(
        str(range_beg) +
        " " +
        str(range_end) +
        " " +
        str(max_cycle) +
        "\n")

# -------------
# collatz_solve
# -------------


def collatz_solve(read: IO[str], write: IO[str]) -> None:
    """
    read a reader
    write a writer
    """
    for input_string in read:
        range_beg, range_end = collatz_read(input_string)
        max_cycle = collatz_eval(range_beg, range_end)
        collatz_print(write, range_beg, range_end, max_cycle)
